<?php
/**
 * @file
 * coh_tasks.features.uuid_term.inc
 */

/**
 * Implements hook_uuid_features_default_terms().
 */
function coh_tasks_uuid_features_default_terms() {
  $terms = array();

  $terms[] = array(
    'name' => 'Patch review',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => '0',
    'uuid' => '1355c9f7-7290-5be4-8d89-1f00ecf73723',
    'vocabulary_machine_name' => 'core_mentoring_task',
    'field_instructions' => array(
      'und' => array(
        0 => array(
          'url' => 'http://drupal.org/node/1488992',
          'title' => 'Review a Drupal core patch',
          'attributes' => array(),
        ),
      ),
    ),
    'field_level' => array(
      'und' => array(
        0 => array(
          'value' => '3',
        ),
      ),
    ),
    'field_queue' => array(
      'und' => array(
        0 => array(
          'url' => 'http://drupal.org/project/issues/search/drupal?status%5B%5D=8&version%5B%5D=8.x&version%5B%5D=7.x&issue_tags=needs+backport+to+D7%2C+Needs+backport+to+7.x',
          'title' => 'Issues marked CNR',
          'attributes' => array(),
        ),
      ),
    ),
    'field_reviewers' => array(),
    'path' => array(
      'alias' => 'core-mentoring-task/patch-review',
      'pathauto' => FALSE,
    ),
  );
  $terms[] = array(
    'name' => 'Triage and find duplicates',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => '0',
    'uuid' => '250ac86c-c939-a104-9de1-a9a637bce854',
    'vocabulary_machine_name' => 'core_mentoring_task',
    'field_instructions' => array(
      'und' => array(
        0 => array(
          'url' => 'http://drupal.org/node/1426582',
          'title' => 'Verify a reported issue',
          'attributes' => array(),
        ),
      ),
    ),
    'field_level' => array(
      'und' => array(
        0 => array(
          'value' => '1',
        ),
      ),
    ),
    'field_queue' => array(
      'und' => array(
        0 => array(
          'url' => 'http://drupal.org/project/issues/search/drupal?order=comment_count&sort=asc&status%5B0%5D=1&status%5B1%5D=13&status%5B2%5D=8&categories%5B0%5D=bug&version%5B0%5D=8.x&version%5B1%5D=7.x&text=&assigned=&submitted=&participant=&issue_tags_op=or&issue_tags=',
          'title' => 'Issues with 0 replies',
          'attributes' => array(),
        ),
      ),
    ),
    'field_reviewers' => array(),
    'path' => array(
      'alias' => 'core-mentoring-task/triage-and-find-duplicates',
      'pathauto' => FALSE,
    ),
  );
  $terms[] = array(
    'name' => 'Issue summary',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => '0',
    'uuid' => '62aa7b3e-5f8b-ae74-95d7-ad07eb2ec6c7',
    'vocabulary_machine_name' => 'core_mentoring_task',
    'field_instructions' => array(
      'und' => array(
        0 => array(
          'url' => 'http://drupal.org/node/1427826',
          'title' => 'Write an issue summary',
          'attributes' => array(),
        ),
      ),
    ),
    'field_level' => array(
      'und' => array(
        0 => array(
          'value' => '1',
        ),
      ),
    ),
    'field_queue' => array(
      'und' => array(
        0 => array(
          'url' => 'http://drupal.org/project/issues/search/drupal?status%5B%5D=1&status%5B%5D=13&status%5B%5D=8&status%5B%5D=14&version%5B%5D=8.x&version%5B%5D=7.x&issue_tags=Issue+summary+initiative',
          'title' => 'Issues tagged with "Issue summary initiative"',
          'attributes' => array(),
        ),
      ),
    ),
    'field_reviewers' => array(),
    'path' => array(
      'alias' => 'core-mentoring-task/issue-summary',
      'pathauto' => FALSE,
    ),
  );
  $terms[] = array(
    'name' => 'Document API changes',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => '0',
    'uuid' => 'aac2f87d-67f1-dd84-e90f-1ce1d6419939',
    'vocabulary_machine_name' => 'core_mentoring_task',
    'field_instructions' => array(),
    'field_level' => array(
      'und' => array(
        0 => array(
          'value' => '3',
        ),
      ),
    ),
    'field_queue' => array(
      'und' => array(
        0 => array(
          'url' => 'http://drupal.org/project/issues/search/drupal?status%5B%5D=1&status%5B%5D=13&status%5B%5D=8&status%5B%5D=14&version%5B%5D=8.x&version%5B%5D=7.x&issue_tags=Issue+summary+initiative%2C+Needs+change+notification',
          'title' => 'Issues tagged with "Needs change notification" or "Issue summary initiative"',
          'attributes' => array(),
        ),
      ),
    ),
    'field_reviewers' => array(),
    'path' => array(
      'alias' => 'core-mentoring-task/document-api-changes',
      'pathauto' => FALSE,
    ),
  );
  $terms[] = array(
    'name' => 'Manual testing',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => '0',
    'uuid' => 'bf5823a5-06d6-4b74-4540-cc7c5b25d9a2',
    'vocabulary_machine_name' => 'core_mentoring_task',
    'field_instructions' => array(
      'und' => array(
        0 => array(
          'url' => 'http://drupal.org/node/1489010',
          'title' => 'Manually test a patch for a Drupal issue',
          'attributes' => array(),
        ),
      ),
    ),
    'field_level' => array(
      'und' => array(
        0 => array(
          'value' => '2',
        ),
      ),
    ),
    'field_queue' => array(
      'und' => array(
        0 => array(
          'url' => 'http://drupal.org/project/issues/search/drupal?status%5B%5D=8&version%5B%5D=8.x&version%5B%5D=7.x&issue_tags=Needs+manual+testing%2C+Needs+screenshot',
          'title' => 'Issues tagged with "Needs manual testing" or "Needs screenshot"',
          'attributes' => array(),
        ),
      ),
    ),
    'field_reviewers' => array(),
    'path' => array(
      'alias' => 'core-mentoring-task/manual-testing',
      'pathauto' => FALSE,
    ),
  );
  $terms[] = array(
    'name' => 'Create patch',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => '0',
    'uuid' => 'e9ac2adc-60f7-f944-2dac-046744f64ec2',
    'vocabulary_machine_name' => 'core_mentoring_task',
    'field_instructions' => array(
      'und' => array(
        0 => array(
          'url' => 'http://drupal.org/node/1424598',
          'title' => 'Create a Drupal core patch',
          'attributes' => array(),
        ),
      ),
    ),
    'field_level' => array(
      'und' => array(
        0 => array(
          'value' => '3',
        ),
      ),
    ),
    'field_queue' => array(
      'und' => array(
        0 => array(
          'url' => 'http://drupal.org/project/issues/search/drupal?status%5B%5D=1&status%5B%5D=13&status%5B%5D=8&version%5B%5D=8.x&version%5B%5D=7.x&issue_tags=Novice',
          'title' => 'Novice issues',
          'attributes' => array(),
        ),
      ),
    ),
    'field_reviewers' => array(),
    'path' => array(
      'alias' => 'core-mentoring-task/create-patch',
      'pathauto' => FALSE,
    ),
  );
  $terms[] = array(
    'name' => 'Document UI changes',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => '0',
    'uuid' => 'ebeb7441-c322-8cc4-c5f8-6dc55c4bd23b',
    'vocabulary_machine_name' => 'core_mentoring_task',
    'field_instructions' => array(),
    'field_level' => array(
      'und' => array(
        0 => array(
          'value' => '2',
        ),
      ),
    ),
    'field_queue' => array(
      'und' => array(
        0 => array(
          'url' => 'http://drupal.org/project/issues/search/drupal?status%5B%5D=1&status%5B%5D=13&status%5B%5D=8&status%5B%5D=14&version%5B%5D=8.x&version%5B%5D=7.x&issue_tags=Issue+summary+initiative%2C+Needs+change+notification',
          'title' => 'Issues tagged with "Needs change notification" or "Issue summary initiative"',
          'attributes' => array(),
        ),
      ),
    ),
    'field_reviewers' => array(),
    'path' => array(
      'alias' => 'core-mentoring-task/document-ui-changes',
      'pathauto' => FALSE,
    ),
  );
  $terms[] = array(
    'name' => 'Write tests',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => '0',
    'uuid' => 'eeb8a977-e7c8-f6c4-d53d-d45545292071',
    'vocabulary_machine_name' => 'core_mentoring_task',
    'field_instructions' => array(
      'und' => array(
        0 => array(
          'url' => 'http://drupal.org/node/1468170',
          'title' => 'Write a Drupal core automated test',
          'attributes' => array(),
        ),
      ),
    ),
    'field_level' => array(
      'und' => array(
        0 => array(
          'value' => '3',
        ),
      ),
    ),
    'field_queue' => array(
      'und' => array(
        0 => array(
          'url' => 'http://drupal.org/project/issues/search/drupal?status%5B%5D=1&status%5B%5D=13&status%5B%5D=8&version%5B%5D=8.x&version%5B%5D=7.x&issue_tags=Needs+tests',
          'title' => 'Issues tagged with "Needs tests"',
          'attributes' => array(),
        ),
      ),
    ),
    'field_reviewers' => array(),
    'path' => array(
      'alias' => 'core-mentoring-task/write-tests',
      'pathauto' => FALSE,
    ),
  );
  $terms[] = array(
    'name' => 'Reroll',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => '0',
    'uuid' => 'eed29df3-b746-9574-3d03-ad8ec44fcab4',
    'vocabulary_machine_name' => 'core_mentoring_task',
    'field_instructions' => array(
      'und' => array(
        0 => array(
          'url' => 'http://drupal.org/patch/reroll',
          'title' => 'Re-rolling patches',
          'attributes' => array(),
        ),
      ),
    ),
    'field_level' => array(
      'und' => array(
        0 => array(
          'value' => '2',
        ),
      ),
    ),
    'field_queue' => array(
      'und' => array(
        0 => array(
          'url' => 'http://drupal.org/project/issues/search/drupal?issue_tags=Needs+reroll',
          'title' => 'Issues tagged with "Needs reroll"',
          'attributes' => array(),
        ),
      ),
    ),
    'field_reviewers' => array(),
    'path' => array(
      'alias' => 'core-mentoring-task/reroll',
      'pathauto' => FALSE,
    ),
  );
  $terms[] = array(
    'name' => 'Backport',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => '0',
    'uuid' => 'f1997d06-b1a6-eca4-11cd-81c1eba06964',
    'vocabulary_machine_name' => 'core_mentoring_task',
    'field_instructions' => array(
      'und' => array(
        0 => array(
          'url' => 'http://drupal.org/node/1538380',
          'title' => 'Backport a Drupal Core patch',
          'attributes' => array(),
        ),
      ),
    ),
    'field_level' => array(
      'und' => array(
        0 => array(
          'value' => '2',
        ),
      ),
    ),
    'field_queue' => array(
      'und' => array(
        0 => array(
          'url' => 'http://drupal.org/project/issues/search/drupal?status%5B%5D=15&version%5B%5D=8.x&version%5B%5D=7.x',
          'title' => 'Issues with status "Patch (to be ported)',
          'attributes' => array(),
        ),
      ),
    ),
    'field_reviewers' => array(),
    'path' => array(
      'alias' => 'core-mentoring-task/backport',
      'pathauto' => FALSE,
    ),
  );
  $terms[] = array(
    'name' => 'Steps to reproduce',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => '0',
    'uuid' => 'f4e35878-9ff2-c5d4-8d30-c989e4432653',
    'vocabulary_machine_name' => 'core_mentoring_task',
    'field_instructions' => array(
      'und' => array(
        0 => array(
          'url' => 'http://drupal.org/node/1468198',
          'title' => 'Document STR',
          'attributes' => array(),
        ),
      ),
    ),
    'field_level' => array(
      'und' => array(
        0 => array(
          'value' => '1',
        ),
      ),
    ),
    'field_queue' => array(
      'und' => array(
        0 => array(
          'url' => 'http://drupal.org/project/issues/search/drupal?status%5B%5D=1&status%5B%5D=13&status%5B%5D=8&version%5B%5D=8.x&version%5B%5D=7.x&issue_tags=Needs+steps+to+reproduce',
          'title' => 'Issues tagged with "Needs steps to reproduce"',
          'attributes' => array(),
        ),
      ),
    ),
    'field_reviewers' => array(),
    'path' => array(
      'alias' => 'core-mentoring-task/steps-reproduce',
      'pathauto' => FALSE,
    ),
  );
  $terms[] = array(
    'name' => 'Improve patch docs/code style',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => '0',
    'uuid' => 'f9214c9b-8a92-0954-b18c-452ea9cdcec9',
    'vocabulary_machine_name' => 'core_mentoring_task',
    'field_instructions' => array(
      'und' => array(
        0 => array(
          'url' => 'http://drupal.org/node/1487976',
          'title' => 'Improve patch coding style and documentation',
          'attributes' => array(),
        ),
      ),
    ),
    'field_level' => array(
      'und' => array(
        0 => array(
          'value' => '2',
        ),
      ),
    ),
    'field_queue' => array(
      'und' => array(
        0 => array(
          'url' => 'http://drupal.org/project/issues/search/drupal?status%5B%5D=13&status%5B%5D=8&version%5B%5D=8.x&version%5B%5D=7.x&issue_tags=needs+backport+to+D7%2C+Needs+backport+to+7.x',
          'title' => 'Issues marked CNR or CNW',
          'attributes' => array(),
        ),
      ),
    ),
    'field_reviewers' => array(),
    'path' => array(
      'alias' => 'core-mentoring-task/improve-patch-docscode-style',
      'pathauto' => FALSE,
    ),
  );
  $terms[] = array(
    'name' => 'Draft change notification',
    'description' => '',
    'format' => 'filtered_html',
    'weight' => '0',
    'uuid' => 'f94a99c8-9e18-0524-7541-247e8d6f8c27',
    'vocabulary_machine_name' => 'core_mentoring_task',
    'field_instructions' => array(
      'und' => array(
        0 => array(
          'url' => 'http://drupal.org/node/1487226',
          'title' => 'Write a change record',
          'attributes' => array(),
        ),
      ),
    ),
    'field_level' => array(
      'und' => array(
        0 => array(
          'value' => '2',
        ),
      ),
    ),
    'field_queue' => array(
      'und' => array(
        0 => array(
          'url' => 'http://drupal.org/project/issues/search/drupal?status%5B%5D=1&status%5B%5D=13&status%5B%5D=8&version%5B%5D=8.x&version%5B%5D=7.x&issue_tags=Needs+change+notification',
          'title' => 'Issues tagged with "Needs change notification"',
          'attributes' => array(),
        ),
      ),
    ),
    'field_reviewers' => array(),
    'path' => array(
      'alias' => 'core-mentoring-task/draft-change-notification',
      'pathauto' => FALSE,
    ),
  );
  return $terms;
}
